var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
//add
var clients = {};

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    //add
    clients[socket.id] = socket;
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    //add
    obdelajZahtevoZaOsebnoSporocilo(socket, vzdevkiGledeNaSocket, clients);
    obdelajPridruzitevKanalu(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    
    //dodano
    socket.on('uporabniki', function() {
      var uporabnikiNaKanalu = io.sockets.clients(trenutniKanal[socket.id]);
      var uporabniki = [];
      for (var i in uporabnikiNaKanalu) {
        var uporabnikSocketId = uporabnikiNaKanalu[i].id;
        //uporabniki.push(vzdevkiGledeNaSocket[uporabnikSocketId]);
        uporabniki[uporabniki.length] = vzdevkiGledeNaSocket[uporabnikSocketId];
      }
      socket.emit('uporabniki', uporabniki);
    });
    
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  var vzdevek = vzdevkiGledeNaSocket[socket.id];
  socket.emit('pridruzitevOdgovor', {
    vzdevek: vzdevek,
    kanal: kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        var kanal = trenutniKanal[socket.id];
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek,
          kanal : kanal
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

//add
function obdelajZahtevoZaOsebnoSporocilo(socket, vzdevkiGledeNaSocket, clients) {
  socket.on('zasebnoSporociloZahteva', function(prejemnik, vsebina) {
    var socketId = 0;
    for (var key in vzdevkiGledeNaSocket) {
      if (vzdevkiGledeNaSocket[key] == prejemnik){
        socketId = key;
        break;
      }
    }
    //sam sebi
    if (socket.id == socketId) {
      socket.emit('zasebnoSporociloOdgovor', {
          uspesno: false,
          vsebina: vsebina,
          prejemnik : prejemnik
        });
    }
    //ni nasel prejemnika
    else if (socketId == 0){
      socket.emit('zasebnoSporociloOdgovor', {
          uspesno: false,
          vsebina: vsebina,
          prejemnik : prejemnik
        });
    }
    else{
      var socketPravi = clients[socketId];
      socketPravi.emit('sporocilo', {
        besedilo : vzdevkiGledeNaSocket[socket.id] + ' (zasebno): ' + vsebina
      });
      socket.emit('zasebnoSporociloOdgovor', {
          uspesno: true,
          vsebina: vsebina,
          prejemnik : prejemnik
        });
    }
    
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    socket.leave(trenutniKanal[socket.id]);
    pridruzitevKanalu(socket, kanal.novKanal);
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}
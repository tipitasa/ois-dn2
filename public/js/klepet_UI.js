function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}


function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    
    sporocilo = sporocilo.replace(/</g, "&lt;");
    sporocilo = sporocilo.replace(/>/g, "&gt;");
    sporocilo = sporocilo.replace(/;\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png">');
    sporocilo = sporocilo.replace(/:\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png">');
    sporocilo = sporocilo.replace(/\(y\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png">');
    sporocilo = sporocilo.replace(/:\*/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png">');
    sporocilo = sporocilo.replace(/:\(/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png">');

    
    var kanalString = $('#kanal').text();
    if (kanalString.indexOf("@") != -1) { //vsebuje @
      var n = kanalString.indexOf("@");
      klepetApp.posljiSporocilo(kanalString.substring(n+2), sporocilo);
    } else {
      klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    }
    
    //lepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    
    sporocilo = '<span class = "uporSporocilo">' + sporocilo + '</span>';
    $('#sporocila').append(divElementEnostavniTekst(sporocilo)).change();
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.'
      $('#kanal').text(rezultat.vzdevek + " @ " + rezultat.kanal);
      
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  //add
  socket.on('zasebnoSporociloOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo =  '(zasebno za ' +rezultat.prejemnik+ '): ' +rezultat.vsebina;
    } else {
      sporocilo = 'Sporočila ' +rezultat.vsebina+ ' uporabniku z vzdevkom ' +rezultat.prejemnik + ' ni bilo mogoče posredovati.'
    }
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.vzdevek + " @ " + rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);

    $('#sporocila').append(novElement).change();
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      
  
      
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  //dodano
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();

    for(var uporabnik in uporabniki) {
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[uporabnik]));
    }
  });
  
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
  
  var xmlhttp=new XMLHttpRequest();
  xmlhttp.open("GET","swearWords.xml",false);
  xmlhttp.send(); var xmlDoc=xmlhttp.responseXML;
  $( "#sporocila" ).on( "change", (function() { 
    //$("#sporocila").val($("#sporocila").text());
    var words = xmlDoc.getElementsByTagName("word");
    var elements = document.getElementsByTagName("span");
    for (var key = 0; key < elements.length; key ++) {
      var cenzura="";
      for(var j=0;j<words.length;j++){ 
        cenzura="";
        var word= words[j].childNodes[0].nodeValue;
        for(var i=0;i<word.length;i++){
          cenzura=cenzura+"*";
    }
    elements[key].textContent = elements[key].textContent.replace(new RegExp("^" + word + "$", "gi"),cenzura);
    }
  }
  }));
  
});